module.exports = {
  //
  outputDir: 'vue3/vue3-ts-cms',
  // 服务器不能开启这个配置
  publicPath: './',
  productionSourceMap: false,
  // 1. 合并到 最终配置中
  configureWebpack: {
    resolve: {
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
      alias: {
        components: '@/components'
      }
    }
    // plugins: [
    //   // 自动引入 element-plus 组件
    //   Components({
    //     resolvers: [ElementPlusResolver()]
    //   })
    // ]
  },
  devServer: {
    port: 8088,
    proxy: {
      '^/api': {
        // target: 'http://152.136.185.210:5000/',
        target: 'https://bookbook.cc/api/vue3-ts-cms',
        // target: 'http://localhost:3000/api/vue3-ts-cms',
        pathRewrite: {
          '^/api': ''
        },
        changeOrigin: true
      }
    }
  }
  // 2.通过传递过来的参数 替换内部的配置
  // configureWebpack: (config) => {
  //   config.resolve.alias = {
  //     '@': path.resolve(__dirname, 'src'),
  //     components: '@/components'
  //   }
  // }
  // 3.链式 通过传递过来的参数 替换内部的配置
  // chainWebpack: (config) => {
  //   config.resolve.alias.set('@', path.resolve(__dirname, 'src')).set('components', '@/components')
  // }
}
