import { useStore } from '@/store/'

export function usePermission(permission: string) {
  const store = useStore()

  const permissions = store.state.login.permissions

  permission = 'system:' + permission

  const verifyResult = permissions.includes(permission)
  return verifyResult
}
