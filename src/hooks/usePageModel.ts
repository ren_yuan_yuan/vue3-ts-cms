import { ref } from 'vue'

import PageModel from '@/components/PageModel'

type CallbackFn = (data?: any) => void

export function usePageModel(newCallback?: CallbackFn, editCallback?: CallbackFn) {
  const pageModelRef = ref<InstanceType<typeof PageModel>>()
  const defaultInfo = ref({})

  // 新增 编辑操作
  const handleNewClick = () => {
    if (pageModelRef.value) {
      pageModelRef.value.dialogVisible = true
      defaultInfo.value = {}
    }
    newCallback && newCallback()
  }

  const handleEditClick = (row: any) => {
    if (pageModelRef.value) {
      defaultInfo.value = { ...row }
      pageModelRef.value.dialogVisible = true
    }
    editCallback && editCallback(row)
  }

  return { pageModelRef, defaultInfo, handleNewClick, handleEditClick }
}
