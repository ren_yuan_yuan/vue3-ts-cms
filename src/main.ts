import { createApp } from 'vue'
import App from './App.vue'

import router from './router'
import store from './store'
import { setupStore } from './store'

import { globalRegister } from './global'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import './assets/css/base.css'
import 'normalize.css'

const app = createApp(App)

app.use(store)
app.use(globalRegister)
setupStore()
app.use(router)
app.use(ElementPlus)

app.mount('#app')
