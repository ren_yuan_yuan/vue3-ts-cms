/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

// 生命用到的 常量名称
declare let $store: any
declare module '*.json'
// declare const NODE_ENV: string
// declare const VUE_APP_MY_NAME: string
