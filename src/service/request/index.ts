import axios from 'axios'
import type { AxiosInstance } from 'axios'
import { CustomRequestConfig, RequestInterceptors } from './types'

import { ElLoading } from 'element-plus'
import type { ILoadingInstance } from 'element-plus'

const DEFAULT_LOADING = false

class request {
  instance: AxiosInstance
  interceptors?: RequestInterceptors
  isLoading: boolean
  loading?: ILoadingInstance

  constructor(config: CustomRequestConfig) {
    // 创建实例
    this.instance = axios.create(config)

    // 不同的实例可以配置是否 默认显示 loading
    this.isLoading = config.isLoading ?? DEFAULT_LOADING
    // 保存 拦截器信息
    this.interceptors = config.interceptors

    // 配置实例拦截器
    this.instance.interceptors.request.use(
      this.interceptors?.requestInterceptor,
      this.interceptors?.requestInterceptorCatch
    )
    this.instance.interceptors.response.use(
      this.interceptors?.responseInterceptor,
      this.interceptors?.responseInterceptorCatch
    )

    // 配置全局拦截器
    this.instance.interceptors.request.use(
      (config) => {
        // console.log('全局请求成功拦截')

        if (this.isLoading) {
          // 添加 loading 效果
          this.loading = ElLoading.service({
            lock: true,
            text: '正在请求',
            background: 'rgba(0,0,0,0.5)'
          })
        }

        return config
      },
      (err) => {
        // console.log('全局请求失败拦截')
        return err
      }
    )

    this.instance.interceptors.response.use(
      (res) => {
        // console.log('全局响应成功拦截')

        this.loading?.close()

        return res.data
      },
      (err) => {
        // console.log('全局响应失败拦截')

        // 移除 loading 效果
        this.loading?.close()

        return err
      }
    )
  }

  request<T = any>(config: CustomRequestConfig<T>): Promise<T> {
    return new Promise((resolve, reject) => {
      // 1. 处理请求拦截器
      if (config.interceptors?.requestInterceptor) {
        config = config.interceptors.requestInterceptor(config)
      }

      // 2.根据请求判断是否需要 loading 效果
      if (config.isLoading === true) {
        this.isLoading = config.isLoading
      }
      this.instance
        .request<any, T>(config)
        .then((res) => {
          // 3. 处理响应拦截器
          if (config.interceptors?.responseInterceptor) {
            res = config.interceptors.responseInterceptor(res)
          }
          // 4. 返回结果
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
        .finally(() => {
          // 不管成功还是失败 都初始化 loading 状态
          this.isLoading = DEFAULT_LOADING
        })
    })
  }
  get<T = any>(config: CustomRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'GET' })
  }
  post<T = any>(config: CustomRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'POST' })
  }
  delete<T = any>(config: CustomRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'DELETE' })
  }
  patch<T = any>(config: CustomRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'PATCH' })
  }
}

export default request
