import type { AxiosRequestConfig, AxiosResponse } from 'axios'

// 请求拦截
export interface RequestInterceptors<T = AxiosResponse> {
  requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestInterceptorCatch?: (error: any) => any
  responseInterceptor?: (res: T) => T
  responseInterceptorCatch?: (error: any) => any
}

// 继承,扩展自己的类型
export interface CustomRequestConfig<T = AxiosResponse>
  extends AxiosRequestConfig {
  interceptors?: RequestInterceptors<T>
  isLoading?: boolean
}
