let BASE_URL = ''
const TIME_OUT = 10000

if (process.env.NODE_ENV === 'development') {
  // BASE_URL = 'http://152.136.185.210:5000'
  BASE_URL = '/api'
} else {
  // production
  BASE_URL = 'https://bookbook.cc/api/vue3-ts-cms'
}

export { BASE_URL, TIME_OUT }
