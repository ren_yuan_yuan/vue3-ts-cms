import request from '@/service/index'

import type { IAccount, ILoginResult } from './types'
import type { IDataType } from '@/service/types'

enum LoginAPI {
  AccountLogin = '/login',
  UserInfo = '/users/', // '/user/:id'
  UserMenus = '/role/' // '/role/:id/menu'
}
// 登录
export function accountLoginRequest(account: IAccount) {
  return request.post<IDataType<ILoginResult>>({
    url: LoginAPI.AccountLogin,
    data: account,
    isLoading: true
  })
}

// 根据ID,获取用户信息
export function GetUserInfoByID(id: number) {
  return request.get<IDataType>({
    url: LoginAPI.UserInfo + id
  })
}

// 根据角色ID,请求用户菜单
export function GetUserMenuByRoleID(id: number) {
  return request.get<IDataType>({
    url: LoginAPI.UserMenus + id + '/menu'
  })
}
