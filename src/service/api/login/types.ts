// 登录接口 发起请求类型
export interface IAccount {
  name: string
  pasword: string
}

// 登录接口 返回值类型
export interface ILoginResult {
  id: number
  name: string
  token: string
}
