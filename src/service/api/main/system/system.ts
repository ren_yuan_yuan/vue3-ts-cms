import request from '@/service/'

import { IDataType } from '@/service/types'

/**
 *
 * @param url 请求路径
 * @param queryInfo 请求分页参数
 * @returns IDataType
 */
export function getPageListData(url: string, queryInfo: any = { offset: 0, size: 1000 }) {
  return request.post<IDataType>({
    url,
    data: queryInfo
  })
}

// url: /users/id
export function deletePageData(url: string) {
  return request.delete<IDataType>({
    url
  })
}

export function createPageData(url: string, newData: any) {
  return request.post<IDataType>({
    url,
    data: newData
  })
}

export function editPageData(url: string, editData: any) {
  return request.patch<IDataType>({
    url,
    data: editData
  })
}
