import request from '@/service/'
import type { IDataType } from '@/service/types'

enum DashboardAPI {
  categoryGoodsCount = '/goods/category/count',
  categoryGoodsSale = '/goods/category/sale',
  categoryGoodsFavor = '/goods/category/favor',
  addressGoodsSale = '/goods/address/sale',
  AmountList = '/goods/amount/list'
}

// 分类商品数量
export function getCategoryGoodsCount() {
  return request.get<IDataType>({
    url: DashboardAPI.categoryGoodsCount
  })
}

// 分类商品的销量
export function getCategoryGoodsSale() {
  return request.get<IDataType>({
    url: DashboardAPI.categoryGoodsSale
  })
}

// 分类商品的收藏
export function getCategoryGoodsFavor() {
  return request.get<IDataType>({
    url: DashboardAPI.categoryGoodsFavor
  })
}

// 不同城市的销量
export function getAddressGoodsSale() {
  return request.get<IDataType>({
    url: DashboardAPI.addressGoodsSale
  })
}

// 商品数据统计的数量

export function getAmountList() {
  return request.get<IDataType>({
    url: DashboardAPI.AmountList
  })
}
