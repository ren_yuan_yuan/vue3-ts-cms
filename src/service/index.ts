import $request from './request/index'
import { BASE_URL, TIME_OUT } from './request/config'

import localCache from '@/utils/localCache'

const request = new $request({
  baseURL: BASE_URL,
  timeout: TIME_OUT,
  // 单个实例的 拦截
  interceptors: {
    requestInterceptor: (config) => {
      // console.log('局部请求成功拦截--')
      const token = localCache.getCache('token')
      if (token) {
        config.headers.Authorization = `Bearer ${token}`
      }
      return config
    },
    requestInterceptorCatch: (err) => {
      // console.log('局部请求失败拦截')
      return err
    },
    responseInterceptor: (res) => {
      // console.log('局部响应成功拦截')
      return res
    },
    responseInterceptorCatch: (err) => {
      // console.log('局部响应失败拦截')
      return err
    }
  }
})

export default request

// export default new request()
