export const ContentConfig = {
  title: '用户列表',
  propList: [
    { prop: 'name', label: '用户名', width: '130px' },
    { prop: 'realname', label: '真实姓名', width: '130px' },
    { prop: 'cellphone', label: '手机号码', width: '130px' },
    { prop: 'enable', label: '状态', width: '160px', slotName: 'enable' },
    { prop: 'createAt', label: '创建时间', width: 'auto', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', width: 'auto', slotName: 'updateAt' },
    { label: '操作', width: '120px', slotName: 'handler' }
  ],
  // 显示序号
  showIndexColumn: true,
  // 显示多选
  showSelectColumn: true
}
