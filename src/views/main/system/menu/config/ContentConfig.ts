export const ContentConfig = {
  title: '菜单列表',
  propList: [
    { prop: 'name', label: '菜单名称' },
    { prop: 'type', label: '级别' },
    { prop: 'url', label: '菜单url' },
    { prop: 'icon', label: '图标icon' },
    { prop: 'permission', label: '按钮权限' },
    { prop: 'createAt', label: '创建时间', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', slotName: 'updateAt' },
    { label: '操作', slotName: 'handler' }
  ],
  // 显示序号
  showIndexColumn: false,
  // 显示多选
  showSelectColumn: false,
  // 菜单展开逻辑
  childrenProps: {
    rowKey: 'id',
    treeProps: {
      children: 'children'
    }
  },
  // 是否显示 footer
  showFooter: false,
  // 显示边框
  showBorder: false,
  // 默认 列的对齐方式
  defaultAlign: 'left'
}
