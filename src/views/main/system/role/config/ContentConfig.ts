export const ContentConfig = {
  title: '角色列表',
  propList: [
    { prop: 'name', label: '角色名称', width: '130px' },
    { prop: 'intro', label: '权限介绍', width: '130px' },
    { prop: 'createAt', label: '创建时间', width: 'auto', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', width: 'auto', slotName: 'updateAt' },
    { label: '操作', width: '120px', slotName: 'handler' }
  ],
  // 显示序号
  showIndexColumn: true,
  // 显示多选
  showSelectColumn: true
}
