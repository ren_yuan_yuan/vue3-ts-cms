export const ContentConfig = {
  title: '商品列表',
  propList: [
    { prop: 'name', label: '商品名称', width: '220px' },
    { prop: 'oldPrice', label: '原价格', width: '80px', slotName: 'oldPrice' },
    { prop: 'newPrice', label: '现价格', width: '80px', slotName: 'newPrice' },
    { prop: 'imgUrl', label: '商品图片', width: '150px', slotName: 'image' },
    { prop: 'address', label: '发货地址', width: '100px' },
    { prop: 'status', label: '状态', width: '160px', slotName: 'status' },
    { prop: 'createAt', label: '创建时间', width: 'auto', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', width: 'auto', slotName: 'updateAt' },
    { label: '操作', width: '120px', slotName: 'handler' }
  ],
  // 显示序号
  showIndexColumn: true,
  // 显示多选
  showSelectColumn: true
}
