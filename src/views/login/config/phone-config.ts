// 验证规则
export const rules = {
  phone: [
    {
      required: true,
      message: '请输入手机号码',
      trigger: 'blur'
    },
    {
      min: 6,
      max: 20,
      message: '长度在6到20个字符之间',
      trigger: 'blur'
    }
  ],
  code: [
    {
      required: true,
      message: '请输入验证码',
      trigger: 'blur'
    },
    {
      min: 6,
      max: 6,
      message: '长度为6个字符',
      trigger: 'blur'
    }
  ]
}
