import { Module } from 'vuex'
import router from '@/router/index'

// API接口
import { accountLoginRequest, GetUserInfoByID, GetUserMenuByRoleID } from '@/service/api/login/login'
import localCache from '@/utils/localCache'
import { mapMenusToPermissions, mapMenusToRoutes } from '@/utils/mapMenus'

// interface
import type { IAccount } from '@/service/api/login/types'
import { IRootState } from '../types'
import { ILoginState } from './types'

// Module<S, R> S模块中的State类型 R跟store中的类型
const loginModule: Module<ILoginState, IRootState> = {
  // 命名空间
  namespaced: true,
  state() {
    return {
      token: '',
      userInfo: {},
      userMenus: [],
      permissions: []
    }
  },
  mutations: {
    changeToken(state, token: string) {
      state.token = token
    },
    changeUserInfo(state, userInfo: any) {
      state.userInfo = userInfo
    },
    changeUserMenus(state, userMenus: any) {
      state.userMenus = userMenus

      // 1. 映射到 routes
      const routes = mapMenusToRoutes(userMenus)
      // 2. 添加到 router
      routes.forEach((route) => {
        router.addRoute('main', route)
      })

      // 3. 获取用户按钮的权限
      const permissions = mapMenusToPermissions(userMenus)
      state.permissions = permissions
    },
    clear(state) {
      console.log('logout')
      state.token = ''
      state.userInfo = {}
      state.userMenus = []
    }
  },
  actions: {
    async accountLoginAction({ commit, dispatch }, payload: IAccount) {
      // 1.登录逻辑
      const loginResult = await accountLoginRequest(payload)
      const { id, token } = loginResult.data

      commit('changeToken', token)
      localCache.setCache('token', token)

      // 发送初始化请求 获取 role/department 调用root的 action
      dispatch('getInitialDataAction', null, { root: true })

      // 2.请求用户信息
      const userInfoResult = await GetUserInfoByID(id)
      const userInfo = userInfoResult.data

      commit('changeUserInfo', userInfo)
      localCache.setCache('userInfo', userInfo)

      // 3.请求用户菜单
      const uesrMenusResult = await GetUserMenuByRoleID(userInfo.role.id)
      const userMenus = uesrMenusResult.data

      commit('changeUserMenus', userMenus)
      localCache.setCache('userMenus', userMenus)

      // 4.跳转到首页
      router.push('/main/analysis/overview')
    },
    // 刷新后,重新加载本地数据
    loadLocalInfo({ commit, dispatch }) {
      const token = localCache.getCache('token')
      if (token) {
        commit('changeToken', token)
        // 发送初始化请求 获取 role/department 调用root的 action
        dispatch('getInitialDataAction', null, { root: true })
      }

      const userInfo = localCache.getCache('userInfo')
      if (userInfo) {
        commit('changeUserInfo', userInfo)
      }

      const userMenus = localCache.getCache('userMenus')
      if (userMenus) {
        commit('changeUserMenus', userMenus)
      }
    }
  }
}

export default loginModule
