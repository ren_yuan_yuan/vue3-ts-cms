import { ILoginState } from './login/types'
import { IDashboardState } from './main/analysis/types'
import { ISystemState } from './main/system/types'

export interface IRootState {
  name: string
  age: number
  allDepartment: any[]
  allRole: any[]
  allMenu: any[]
}

export interface IRootWithModule {
  login: ILoginState
  system: ISystemState
  dashboard: IDashboardState
}

// 交叉类型，解决 vuex原生 类型推断不好用问题
export type IStoreType = IRootState & IRootWithModule
