import { createStore, Store, useStore as useVuexStore } from 'vuex'

import { IRootState, IStoreType } from './types'

// 模块
import login from './login/login'
import system from './main/system/system'
import dashboard from './main/analysis/dashboard'

import { getPageListData } from '@/service/api/main/system/system'

const store = createStore<IRootState>({
  state() {
    return {
      name: 'pengsir',
      age: 15,
      allDepartment: [],
      allRole: [],
      allMenu: []
    }
  },
  mutations: {
    changeAllDepartment(state, list) {
      state.allDepartment = list
    },
    changeAllRole(state, list) {
      state.allRole = list
    },
    changeAllMenu(state, list) {
      state.allMenu = list
    }
  },
  getters: {},
  actions: {
    async getInitialDataAction({ commit }) {
      // 1. 请求部门数据
      const departmentData = await getPageListData('/department/list')
      const { list: departmentList } = departmentData.data

      // 2. 请求角色数据
      const roleData = await getPageListData('/role/list')
      const { list: roleList } = roleData.data

      // 3. 请求 角色菜单树
      const menuData = await getPageListData('/menu/list', {})
      const { list: menuList } = menuData.data

      commit('changeAllDepartment', departmentList)
      commit('changeAllRole', roleList)
      commit('changeAllMenu', menuList)
    }
  },
  modules: {
    login,
    system,
    dashboard
  }
})

export function setupStore() {
  store.dispatch('login/loadLocalInfo')
  // 获取 部门和角色数据
  // store.dispatch('getInitialDataAction')
}

export function useStore(): Store<IStoreType> {
  return useVuexStore()
}

export default store
