import { IRootState } from '@/store/types'
import { ISystemState } from './types'
import { Module } from 'vuex'
import { createPageData, deletePageData, editPageData, getPageListData } from '@/service/api/main/system/system'

const systemModule: Module<ISystemState, IRootState> = {
  namespaced: true,
  state() {
    return {
      usersList: [],
      usersCount: 0,
      roleList: [],
      roleCount: 0,
      goodsList: [],
      goodsCount: 0,
      menuList: [],
      menuCount: 0
    }
  },
  mutations: {
    changeUsersList(state, list: any[]) {
      state.usersList = list
    },
    changeUsersCount(state, userCount: number) {
      state.usersCount = userCount
    },
    changeRoleList(state, list: any[]) {
      state.roleList = list
    },
    changeRoleCount(state, roleCount: number) {
      state.roleCount = roleCount
    },
    changeGoodsList(state, list: any[]) {
      state.goodsList = list
    },
    changeGoodsCount(state, roleCount: number) {
      state.goodsCount = roleCount
    },
    changeMenuList(state, list: any[]) {
      state.menuList = list
    },
    changeMenuCount(state, roleCount: number) {
      state.menuCount = roleCount
    }
  },
  getters: {
    pageListData(state) {
      return (pageName: string) => {
        // return state[`${pageName}`]list
        return (state as any)[`${pageName}List`]
        // switch (pageName) {
        //   case 'users':
        //     return state.usersList
        //   case 'role':
        //     return state.roleList
        // }
      }
    },
    pageListCount(state) {
      return (pageName: string) => {
        return (state as any)[`${pageName}Count`]
      }
    }
  },
  actions: {
    // 获取用户
    async getPageListAction({ commit }, payload: any) {
      // 1.获取 pageUrl
      const { pageName } = payload

      const pageUrl = `/${pageName}/list`

      // 2.对页面发送网络请求
      console.log('pageUrl, payload.queryInfo: ', pageUrl, payload.queryInfo)
      const pageResult = await getPageListData(pageUrl, payload.queryInfo)

      // 3.提交 mutations
      const { list, totalCount } = pageResult.data
      const upperName = pageName.slice(0, 1).toUpperCase() + pageName.slice(1)

      commit(`change${upperName}List`, list)
      commit(`change${upperName}Count`, totalCount)
    },
    // 根据id 和 pageName 删除数据
    async deletePageDataAction(context, payload: any) {
      // 1. 拼接参数

      // 1. pageName -> /users/
      // 2. id /users/id
      const { pageName, id } = payload
      const pageUrl = `${pageName}/${id}`

      // 2.发送请求 删除数据
      const res = await deletePageData(pageUrl).catch((err) => console.log('err', err))
      if (res) {
        console.log('res', res)
        // 3.刷新数据
        context.dispatch('getPageListAction', {
          pageName,
          queryInfo: {
            offset: 0,
            size: 10
          }
        })
      }
    },
    // 新增
    async createPageDataAction({ dispatch }, payload: any) {
      // 1.创建数据请求
      const { pageName, newData } = payload

      const pageUrl = `/${pageName}`
      const res = await createPageData(pageUrl, newData).catch((err) => console.log('err', err))
      // 2. 刷新页面

      if (res) {
        dispatch('getPageListAction', {
          pageName,
          queryInfo: {
            offset: 0,
            size: 10
          }
        })
      }
    },
    // 修改
    async editPageDataAction({ dispatch }, payload: any) {
      const { pageName, editData, id } = payload
      const pageUrl = `/${pageName}/${id}`
      const res = await editPageData(pageUrl, editData).catch((err) => console.log('err', err))
      if (res) {
        dispatch('getPageListAction', {
          pageName,
          queryInfo: {
            offset: 0,
            size: 10
          }
        })
      }
    }
  }
}

export default systemModule
