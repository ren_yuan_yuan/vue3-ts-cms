import type { Module } from 'vuex'
import type { IRootState } from '@/store/types'
import type { IDashboardState } from './types'

import {
  getCategoryGoodsCount,
  getCategoryGoodsSale,
  getCategoryGoodsFavor,
  getAddressGoodsSale,
  getAmountList
} from '@/service/api/main/analysis/dashboard'

import { Favor } from './types'

import { randomNumber } from '@/utils/'

const dashboardModule: Module<IDashboardState, IRootState> = {
  namespaced: true,
  state() {
    return {
      categoryGoodsCount: [],
      categoryGoodsSale: [],
      categoryGoodsFavor: {
        labels: [],
        data: [],
        default: []
      },
      addressGoodsSale: [],
      categoryAmountList: [],
      timerId1: null,
      timerId2: null
    }
  },
  mutations: {
    changeCategoryGoodsCount(state, data) {
      state.categoryGoodsCount = data
    },
    changeCategoryGoodsSale(state, data) {
      state.categoryGoodsSale = data
    },
    changeCategoryGoodsFavor(state, data) {
      state.categoryGoodsFavor = data
    },
    changeAddressGoodsSale(state, data) {
      state.addressGoodsSale = data
    },
    changeCategoryAmountList(state, data) {
      state.categoryAmountList = data
    }
  },
  actions: {
    async getDashboarDataAction({ state, commit }, payload) {
      // 1. 分类商品数量
      const { data: categoryGoodsCount } = await getCategoryGoodsCount()
      commit('changeCategoryGoodsCount', categoryGoodsCount)

      // 2. 分类商品的销量
      const { data: categoryGoodsSale } = await getCategoryGoodsSale()
      commit('changeCategoryGoodsSale', categoryGoodsSale)

      // 3. 分类商品的收藏
      const { data: categoryGoodsFavor } = await getCategoryGoodsFavor()
      const EditCategoryGoodsFavor: any = {
        labels: [],
        data: [],
        default: categoryGoodsFavor
      }
      categoryGoodsFavor.forEach((item: any) => {
        EditCategoryGoodsFavor.labels.push(item.name)
        EditCategoryGoodsFavor.data.push(item.goodsFavor)
      })
      commit('changeCategoryGoodsFavor', EditCategoryGoodsFavor)

      // 4. 不同城市的销量
      const { data: addressGoodsSale } = await getAddressGoodsSale()
      commit('changeAddressGoodsSale', addressGoodsSale)

      // 5.   // tips 数据
      const { data: categoryAmountList } = await getAmountList()
      commit('changeCategoryAmountList', categoryAmountList)

      // 让画面动起来
      state.timerId1 && clearInterval(state.timerId1)
      state.timerId1 = setInterval(() => {
        // 1.构造新的
        const newData: any = {
          labels: [],
          data: []
        }
        state.categoryGoodsFavor.labels.forEach((item: any) => {
          newData.labels.push(item)
        })
        state.categoryGoodsFavor.data.forEach((item: any) => {
          newData.data.push(item)
        })
        // 去掉第一项
        // const index = randomNumber(0, categoryGoodsFavor.length)
        // newData.data.splice(index, 1)
        // const currentLabel = newData.labels.splice(index, 1)
        newData.data.shift()
        const currentLabel = newData.labels.shift()

        // 追加一项随机的在后
        newData.labels.push(currentLabel)
        newData.data.push(randomNumber(1000, 20000))

        // 3. 把新的赋值给旧的
        EditCategoryGoodsFavor.labels = newData.labels
        EditCategoryGoodsFavor.data = newData.data

        // 3. 提交
        commit('changeCategoryGoodsFavor', newData)
      }, 3000)

      state.timerId2 && clearInterval(state.timerId1)
      setTimeout(() => {
        state.timerId2 = setInterval(() => {
          // 销量数据统计图 categoryGoodsSale
          const shiftData = state.categoryGoodsSale.shift()
          state.categoryGoodsSale.push({
            name: shiftData.name.slice(0, 2) + randomNumber(1, 99),
            id: shiftData,
            goodsCount: randomNumber(10000, 100000)
          })
          commit('changeAddressGoodsSale', state.categoryGoodsSale)
        }, 3000)
      }, 1500)
    }
  }
}

export default dashboardModule
