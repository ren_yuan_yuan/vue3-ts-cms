export interface IDashboardState {
  // 分类商品数量
  categoryGoodsCount: any[]
  // 分类商品的销量
  categoryGoodsSale: any[]
  // 分类商品的收藏
  categoryGoodsFavor: Favor
  // 不同城市的销量
  addressGoodsSale: any[]
  // tips 数据
  categoryAmountList: any[]
  timerId1: any
  timerId2: any
}

export interface Favor {
  labels: []
  data: []
  default: []
}
