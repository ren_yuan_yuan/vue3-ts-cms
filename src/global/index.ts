import { App } from 'vue'

import registerProperties from './register-properties'

registerProperties

export function globalRegister(app: App) {
  app.use(registerProperties)
}
