import { createRouter, createWebHashHistory } from 'vue-router'
import type { RouteRecordRaw } from 'vue-router'
import localCache from '@/utils/localCache'

import { ElMessage } from 'element-plus'
import { firstMenu } from '@/utils/mapMenus'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/main'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/Login.vue')
  },
  {
    path: '/main',
    name: 'main',
    component: () => import('@/views/main/Main.vue'),
    children: [
      {
        path: ':pathMatch(.*)',
        name: 'not-found',
        component: () => import('@/views/not-found/NotFound.vue')
      }
    ] // 根据 userMenus 决定
  }
  // {
  //   path: '/:pathMatch(.*)',
  //   name: 'not-found',
  //   component: () => import('@/views/not-found/NotFound.vue')
  // }
]

const router = createRouter({
  routes,
  history: createWebHashHistory()
})

router.beforeEach((to) => {
  if (to.path !== '/login') {
    const token = localCache.getCache('token')
    // 如果没有登录的话,跳转到登录页
    if (!token) {
      ElMessage.info({
        message: '请登录哦~'
      })
      return '/login'
    }
  }

  if (to.path === '/main') {
    console.log('to.path: ', to.path)
    // 重定向到第一个菜单下的第一个菜单项
    console.log('firstMenu.url: ', firstMenu)
    return firstMenu.url
  }
})

export default router
