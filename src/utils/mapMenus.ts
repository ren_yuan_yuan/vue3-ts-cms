import type { RouteRecordRaw } from 'vue-router'

// 进入系统后看到的默认选中项
export let firstMenu: any = null

// 动态注册左侧菜单
export function mapMenusToRoutes(userMenus: any[]): RouteRecordRaw[] {
  const routes: RouteRecordRaw[] = []

  // 1.先去加载默认所有的 routes
  const allRoutes: RouteRecordRaw[] = []
  // webpack 工具,帮我们加载文件夹 require.context(路径,递归(深度),规则)
  const routeFiles = require.context('../router/main', true, /\.ts$/)
  // routeFiles.keys() 拿到所有文件路径
  routeFiles.keys().forEach((key) => {
    // commonjs 加载文件
    const route = require('../router/main' + key.split('.')[1])
    allRoutes.push(route.default)
  })

  // 2.根据菜单获取需要添加的 routes
  const recursGetRoute = (menus: any[]) => {
    for (const menu of menus) {
      if (menu.type == 2) {
        // find() 方法返回数组中满足提供的测试函数的第一个元素的值。否则返回 undefined。
        const route = allRoutes.find((route) => route.path == menu.url)
        // 设置默认显示的页面
        firstMenu = firstMenu ?? menu

        if (route) {
          routes.push(route)
        }
      } else {
        recursGetRoute(menu.children)
      }
    }
  }

  recursGetRoute(userMenus)

  return routes
}

export function pathMapBreadcrumbs(userMenus: any[], currentPath: string) {
  const breadcurmbs: any[] = []
  pathMapToMenu(userMenus, currentPath, breadcurmbs)
  return breadcurmbs
}

// 左侧菜单 匹配到对应 id
export function pathMapToMenu(userMenus: any[], currentPath: string, breadcurmbs?: any[]): any {
  for (const menu of userMenus) {
    if (menu.type === 1) {
      const findMenu = pathMapToMenu(menu.children ?? [], currentPath)
      if (findMenu) {
        // 一级目录
        breadcurmbs?.push({ name: menu.name })
        // 二级目录
        breadcurmbs?.push({ name: findMenu.name })
        return findMenu
      }
    } else if (menu.type === 2 && menu.url === currentPath) {
      return menu
    }
  }
}

export function mapMenusToPermissions(userMenus: any[]) {
  const permissions: string[] = []

  const _recurseGetPermission = (menus: any[]) => {
    for (const menu of menus) {
      if (menu.type === 1 || menu.type === 2) {
        _recurseGetPermission(menu.children ?? [])
      } else if (menu.type === 3) {
        permissions.push(menu.permission)
      }
    }
  }
  _recurseGetPermission(userMenus)
  return permissions
}

// 获取叶子节点所有的 id
export function getMenuLeafKeys(menuList: any) {
  const leafKeys: number[] = []
  const halfCheckedKeys: number[] = []
  const _recuseGetLeaf = (menuList: any[]) => {
    for (const menu of menuList) {
      if (menu.children) {
        halfCheckedKeys.push(menu.id)
        _recuseGetLeaf(menu.children)
      } else {
        leafKeys.push(menu.id)
      }
    }
  }
  _recuseGetLeaf(menuList)
  return { leafKeys, halfCheckedKeys }
}
