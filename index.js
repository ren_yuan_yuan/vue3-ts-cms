function getRouters(routers) {
  let result = new Array()
  for (let index = 0; index < routers.length; index++) {
    const element = routers[index]
    //添加children
    const childrens = element.children
    let childArr = new Array()
    for (let i = 0; i < childrens.length; i++) {
      const child = childrens[i]
      debugger
      childArr[i] = {
        path: child.path,
        component: () => require(`@/views/company/index`),
        // component: () => import('@/views/company/index'),
        name: child.name,
        meta: {
          title: child.name,
          icon: child.openImg
        }
      }
    }
    result[index] = {
      path: element.path,
      component: "Layout",
      name: element.name,
      meta: {
        title: element.name,
        icon: element.openImg
      },
      children: childArr
    }
  }
  return result
}
